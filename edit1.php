<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery-3.5.1.min.js"></script> 

    <link rel="stylesheet" href="fontawesome/css/all.min.css">
    <title>Update</title>
</head>
<body>
<?php 
  
  if(isset($_POST["submitbutton"])){
     $pid = $_GET["proid"];
     echo "<script>alert(\"The Id is $pid\")</script>";
     $productname = $stock = $price = $category = $brand = $productid = "";
     $productname = $_POST["productname"];
     $stock = $_POST["stock"];
     $price = $_POST["price"];
     $category = $_POST["category"];
     $description = $_POST["brand"];
     $id=$productid;
       $host="localhost";
       $user="root";
       $pass="";
       $db="woms";
       $str="mysql:host=".$host.";dbname=".$db;
 
       try {
         $con=new PDO($str,$user,$pass);
         $sql="UPDATE products set product_name =:product_name, price =:price, stock_status =:stock_status , product_category =:product_category, description=:description WHERE product_id=:ID";
         $res=$con->prepare($sql);
         $res->execute(["product_name"=>$productname,"price"=>$price,"stock_status"=>$stock,"product_category"=>$category,"description"=>$description,"ID"=>$pid]);
         echo "Data Updated";
         
     } catch (PDOException $e) {
         echo "Connection failed". $e->getMessage();
     }
      header( "Location:index.php");
   }
    
    ?>
<div class="container w-75">
<form action="" method="post">
                 <div class="form-group mt-5 form-row justify-content-center bg-success">
                    <h3>Update Product</h3>
                  </div>
                  <div class="form-group">
                    <label for="name">Product Name</label>
                    <input id="name" class="form-control" type="text" name="productname">
                  </div>
                  <div class="form-group">
                    <label for="price">Price</label>
                    <input id="price" class="form-control" type="text" name="price">
                  </div>
                  <div class="form-group">
                    <label for="stock">Stock</label>
                    <input id="stock" class="form-control" type="text" name="stock">
                  </div>
                  <div class="form-group">
                    <label for="category">Category</label>
                    <input id="category" class="form-control" type="text" name="category">
                  </div>
                  <div class="form-group">
                    <label for="brand">Brand</label>
                    <input id="brand" class="form-control" type="text" name="brand">
                  </div>
                  <div class="form-row justify-content-center">
                    <button type="submit" name="submitbutton" class="btn btn-success">Update</button>
                  </div>
                </form>
                </div>                
</body>
</html>

 