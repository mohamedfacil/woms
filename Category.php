<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Catogory</title>
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="fontawesome/css/all.min.css">
    <script src="js/jquery-3.5.1.min.js"></script>
</head>
<body class="categoryBody">
    <?php require 'nav.php' ?>

   <nav aria-label="breadcrumb" class="moveLeft">
    <ol class="breadcrumb">
      <li class="breadcrumb-item text-primary"><a href="#">WOMS</a></li>
      <li class="breadcrumb-item"><a href="index.html">Products</a></li>
      <li class="breadcrumb-item active" aria-current="page">Product Categories</li>
    </ol>
  </nav>
    
  <div class="row w-75">
      <div class="col-md-6 pt-2 px-5 pb-5">
          <p class="text-justify pfont">Product categories for your store can be managed here, To change the order of categories on the front-end,you can drag and frop to store them. To see more categories listed,click the "Select options" link at the top-right of this page.</p>

          <form action="" method="POST">
              <h4>Add new category</h4>
              <div class="form-group">
                  <label for="name">Name</label>
                  <input id="name" class="form-control" type="text" name="name">
                  <small class="text-muted form-text">The name is how it appears in your site</small>
              </div>
              
            <div class="form-group">
                <label for="desc">Description</label>
                <textarea name="desc" class="form-control" id="desc"></textarea>
                <small class="text-muted form-text">The description is only for your understanding purpose.</small>
            </div>
                <div class=" form-group custom-file mb-3">
                    <input type="file" class="custom-file-input" id="validatedCustomFile" required>
                    <label class="custom-file-label" for="validatedCustomFile">Upload/Add Images</label>
                </div>
                <div class="form-group ">
                    <button type="submit" class="btn btn-primary ">Add new category</button>
                </div>
          </form>
      </div>
      <div class="col-md-6 pt-2 px-5 pb-5">
          <form action="" method="POST"> 
              <div class="form-row justify-content-end mb-3">
                <div class="form-group col-md-6 ">
                    <input type="text" class="form-control" id="in" >
                </div>
                <div class="form-group col-md-2">   
                    <button type="submit" class="btn btn-primary mb-2">Search</button>
                </div>
              </div>
            <div class="form-row">
                
                <div class="col">
                    <select name="ba" class="form-control" id="ba">
                        <option value="bulk action">bulk action</option>
                    </select>
                </div>
                <div class="col">
                    <button class="btn btn-primary">Apply</button>
                </div>
                
                
            </div>   
            <div class="form-row">            
                <table class="table table-striped table-responsive-sm my-4">
                <thead class="thead-light">
                    <th><div class="form-check">
                        <input class="form-check-input" type="checkbox" id="disabledFieldsetCheck" disabled>
                      </div></th>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Count</th>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                    </tr>
                </tbody>
            </table> 
        </div>

            <div class="form-row mb-5">
                
                <div class="col">
                    <select name="ba" class="form-control" id="ba">
                        <option value="bulk action">bulk action</option>
                    </select>
                </div>
                <div class="col">
                    <button class="btn btn-primary">Apply</button>
                </div>
                
                
            </div> 
            <h6>Note:</h6>
            <p class="text-justify pfont">Deleting the categories does not delete the products in that category, instead products that were only assigned to the deleted category are set to the category <b>Uncategorized</b></b></p>
             </div>   
          </form>
      </div>
  </div>

  <script src="js/bootstrap.min.js" ></script>
<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function () {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
});
</script>
</body>
</html>