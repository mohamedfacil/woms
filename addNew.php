<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add New</title>
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="fontawesome/css/all.min.css">
    <script src="js/jquery-3.5.1.min.js"></script>
</head>
<body>
<?php require 'nav.php' ?>

<!-- Page Content  -->


<nav aria-label="breadcrumb " class="moveLeft ">
  <ol class="breadcrumb">
    <li class="breadcrumb-item text-primary"><a href="#">WOMS</a></li>
    <li class="breadcrumb-item"><a href="index.html">Products</a></li>
    <li class="breadcrumb-item active" aria-current="page">AddNew</li>
  </ol>
</nav>




<div class="overlay"></div>


    <div class="main1">
        
        <form action="insert.php" method="post" class="w-75 ml-5">
            <div class="form-group " >
                <label for="inputPName"><h3>Product Details</h3></label>
                <input type="text" class="form-control" name="inputPName" id="inputPName" placeholder="Product Name">
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  
                  <input type="text" class="form-control" name="inputPID4" id="inputPID4" placeholder="Product ID">
                </div>
                <div class="form-group col-md-6">
                 
                  <input type="text" class="form-control" name="inputPrice4" id="inputPrice4" placeholder="Price">
                </div>
              </div>
              <div class=" form-group custom-file mb-3">
                <input type="file" class="custom-file-input" id="validatedCustomFile" required>
                <label class="custom-file-label" for="validatedCustomFile">Product Image...</label>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                    <select id="inputAvail" name="inputAvail" class="form-control">
                        <option selected disabled>Availability</option>
                        <option value="In Stock">In Stock</option>
                        <option value="Out of Stock">Out of Stock</option>
                      </select>
                </div>
                <div class="form-group col-md-6">
                    <select id="inputCatg" name="inputCatg" class="form-control">
                        <option selected>Catogory</option>
                        <option>Catogory 1</option>
                        <option>Catogory 2</option>
                      </select>
                </div>
              </div>
              <div class="form-group">
                <label for="inputText"><h3>Product Description</h3></label>
                <textarea class="form-control" id="inputText" name="inputText" rows="3"></textarea>
              </div>
              <div class="form-group row justify-content-center">
              <button type="submit" class="btn btn-primary my-4">Save</button>
            </div>
        </form>
        
    </div>
  
<!-- Bootstrap JS -->
<script src="js/bootstrap.min.js"></script>
<!-- jQuery Custom Scroller CDN -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

<script type="text/javascript">
$(document).ready(function () {
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

   

    
});
</script>
</body>
</html>