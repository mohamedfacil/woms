<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Product </title>

    
    
    
    <link rel="stylesheet" href="css/style3.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery-3.5.1.min.js"></script> 

    <link rel="stylesheet" href="fontawesome/css/all.min.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

   
</head>

<body>
<?php
    $host="localhost";
    $user="root";
    $pass="";
    $db="woms";
    $str="mysql:host=".$host.";dbname=".$db;

    try {
      $con=new PDO($str,$user,$pass);
      $con->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_OBJ);
      $con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
      $sql="SELECT * from products";
          
        $res=$con->prepare($sql);
        $res->execute();
        $users=$res->fetchAll();
        foreach ($users as $row) {
          echo "<script>$(document).ready(function(){ $(\".tb\").append(`<tr class=\"tbr\"><td><div class=\"form-group\">
            <div class=\"form-check\">
              <input class=\"form-check-input\" type=\"checkbox\" id=\"disabledFieldsetCheck\" disabled>
            </div>
          </div></td><td>$row->id</td><td>$row->product_name <div><a href=\"edit1.php?proid=$row->product_id\">Edit</a>
          <a href=\"delete.php?id=$row->product_id\" type=\"submit\" class=\"text-danger\">Delete</a></div></td><td>$row->product_id</td><td>$row->price</td><td>$row->stock_status</td><td>$row->product_category</td><td>$row->description</td><td>$row->date_of_creation</td></tr>`) });</script>";
        }
            
  } catch (PDOException $e) {
      echo "Connection failed". $e->getMessage();
  }
?>
        

        
         <!-- <div id="my-modal" class="modal fade">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
              <h3>Edit Row</h3>
            </div>  
            <div class="modal-body p-5">
               
              </div>
            </div>
          </div>
        </div> -->
    
        <!-- Sidebar  -->
        <?php require 'nav.php' ?>

        <!-- Page Content  -->
        <nav aria-label="breadcrumb " class="moveLeft">
            <ol class="breadcrumb">
              <li class="breadcrumb-item text-primary"><a href="#">WOMS</a></li>
              <li class="breadcrumb-item active" aria-current="page">Products</li>
            </ol>
          </nav>

        
    

    <div class="overlay"></div>

    <div class="main d-flex  justify-content-end ">
        
        <form action="#" method="post" class="w-75">
            <div class="form-row ">
            <div class="form-group col-md-6">
                <label for="products"><h3>Products</h3></label>
                 <a href="addNew.php" class="btn btn-primary mb-2">Add New</a>
            </div>
            <div class="form-group col-md-2">   
            </div>
            <div class="form-group col-md-3 ">
                <input type="text" class="form-control" id="in" >
            </div>
            <div class="form-group col-md-1">   
                <button type="submit" class="btn btn-primary mb-2">Search</button>
            </div>
            </div>
              <div class="form-row">
                <div class="form-group col-md-3">
                    <select id="inputAvail" name="inputAvail" class="form-control">
                        <option selected disabled>Bulk action</option>
                      </select>
                </div>

                <div class="form-group col-md-2">
                 
                    <button type="submit" class="btn btn-primary mb-2">Apply</button>
                </div>
              </div>
              <div class=" form-row">
                <div class="form-group col-md-3">
                <select id="myInput" name="myInput" onchange="myFunction()" class="form-control">
                        <option selected disabled>Category</option>
                        <option id="myInput" value="Category 1">Category 1</option>
                        <option id="myInput" value="Category 2">Category 2</option>
                      </select>
                </div>
                <div class="form-group col-md-3">
                <select id="myInput1" name="myInput1" onchange="myFunction1()" class="form-control">
                        <option selected disabled>Availability</option>
                        <option  id="myInput1" value="In Stock">In Stock</option>
                        <option  id="myInput1" value="Out of Stock">Out of Stock</option>
                      </select>
                </div>
                <div class="form-group col-md-1">
                    <button type="submit" class="btn btn-primary mb-2">Filter</button>
                </div>
             </div>
              <div class="form-row justify-content-end">
                <ul class="pagination">
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                      </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                      </a>
                    </li>
                  </ul>
              </div>
              <div class="form-row">
                <table class="table table-striped table-responsive-sm mb-5" id="myTable">
                    <thead class="thead-light">
                        <tr>
                            <th><div class="form-group">
                                <div class="form-check">
                                  <input class="form-check-input" type="checkbox" id="disabledFieldsetCheck" disabled>
                                </div>
                              </div></th>
                            <th>#</th>               
                            <th>Name</th>
                            <th>Product ID</th>
                            <th>Price</th>
                            <th>Stock</th>
                            <th>Categories</th>
                            <th>Brand</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody class="tb">
                            
                            
                    </tbody>
                </table>

              </div>
              
        </form>
        
    </div>

   
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#dismiss, .overlay').on('click', function () {
                $('#sidebar').removeClass('active');
                $('.overlay').removeClass('active');
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').addClass('active');
                $('.overlay').addClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>
         
    <!-- <script>
        $(document).ready(function () {
            $(".option").hide();
            $(".tbr").hover(function(){
                
                $(this).toggleClass('opac03');
                $(".option").toggle();
            });
        });
    </script> -->
    <script>
    function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[6];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
function myFunction1() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput1");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[5];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
</script>
</body>

</html>